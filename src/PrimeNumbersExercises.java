public class PrimeNumbersExercises {
    public static boolean isPrime(int n) {
        int i = 2;
        boolean prime = true;

        while (i <= (n / 2) && prime == true) {
            if (n % i == 0) {
                prime = false;
            }
            i = i + 1;
        }
        return prime;
    }

    public static void main(String[] args) {
        int n = 100;
        int i = 1;
        while (i < n) {
            boolean prime = isPrime(i);
            if (prime) {
                System.out.println(i);
            }
            i++;
        }
    }
}
