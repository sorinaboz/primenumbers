/*
 * example of determining if two numbers are prime without using method call
 * */
public class PrimeNumbersNoMethodCall {

//    not used in this version
//    public static boolean isPrime(int n) {
//
//        int i = 2;
//        boolean prime = true;
//
//        while (i <= (n / 2) && prime == true) {
//            if (n % i == 0) {
//                prime = false;
//            }
//            i = i + 1;
//
//        }
//        return prime;
//    }

    public static void main(String[] args) {

//        determining if `myNumber` is prime
        int myNumber = 13;
        int i = 2;
        boolean prime = true;

        while (i < (myNumber / 2) && prime == true) {
            if (myNumber % i == 0) {
                prime = false;
            }
            i = i + 1;

        }
        System.out.println(prime);
//        -----------
//        for the second number we have to write the algorithm again
        int mySecondNumber = 35;
        i = 2;
        boolean p2 = true;

        while (i < (mySecondNumber / 2) && p2 == true) {
            if (mySecondNumber % i == 0) {
                p2 = false;
            }
            i = i + 1;

        }
        System.out.println(p2);
    }
}
